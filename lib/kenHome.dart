import 'package:flutter/material.dart';

class KenHome extends StatefulWidget {
  KenHome({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _KenHomeState createState() => _KenHomeState();
}

class _KenHomeState extends State<KenHome> {
  int _page = 0;
  PageController _c;
  @override
  void initState() {
    _c = new PageController(
      initialPage: _page,
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      bottomNavigationBar: new BottomNavigationBar(
        currentIndex: _page,
        onTap: (index) {
          this._c.animateToPage(index,
              duration: const Duration(milliseconds: 500),
              curve: Curves.easeInOut);
        },
        items: <BottomNavigationBarItem>[
          new BottomNavigationBarItem(
              icon: new Icon(Icons.supervised_user_circle),
              title: new Text("Users")),
          new BottomNavigationBarItem(
              icon: new Icon(Icons.notifications), title: new Text("Alerts")),
          new BottomNavigationBarItem(
              icon: new Icon(Icons.email), title: new Text("Inbox")),
        ],
      ),
      body: new PageView(
        controller: _c,
        onPageChanged: (newPage) {
          setState(() {
            this._page = newPage;
          });
        },
        children: <Widget>[
          new Center(
            child: new Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[SizedBox(height: 100.0), cards],
            ),
          ),
          new Center(
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                new Icon(Icons.notifications),
                new Text("Alerts")
              ],
            ),
          ),
          new Center(
            child: new Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[new Icon(Icons.mail), new Text("Inbox")],
            ),
          ),
        ],
      ),
      backgroundColor: Colors.orange,
    );
  }

  void leftButtonPressed() {
    setState(() {});
  }

  final cards = new Center(
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        new Card(
          margin: new EdgeInsets.only(
              left: 20.0, right: 20.0, top: 8.0, bottom: 5.0),
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
          elevation: 4.0,
          child: new Column(
            children: <Widget>[
              new Container(
                child: ListTile(
                  title: Text(
                    "Balance",
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
                  ),
                  trailing: Text(
                    "Rp.800000",
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 20.0),
                  ),
                ),
              ),
              new UserAccountsDrawerHeader(
                accountName: new Text('Basarudin',
                    textAlign: TextAlign.center,
                    style: TextStyle(color: Colors.black)),
                accountEmail: new Text(""),
                decoration: new BoxDecoration(
                  color: Colors.white,
                  // image: new DecorationImage(
                  //   // image: new ExactAssetImage('assets/gambar.jpg'),
                  //   fit : BoxFit.cover,
                  // )
                ),
                currentAccountPicture: new CircleAvatar(
                    backgroundImage: NetworkImage(
                        'https://s3.amazonaws.com/ionic-io-static/KMFRjXpQYyZ7DiB8IwJX_udin.jpeg')),
              ),
            ],
          ),
        ),
        new Container(
          margin: new EdgeInsets.only(
              left: 20.0, right: 20.0, top: 8.0, bottom: 5.0),
          child: new UserAccountsDrawerHeader(
            accountName: new Padding(
              padding: const EdgeInsets.only(left: 20.0, right: 20.0, top: 8.0, bottom: 5.0),
              child: Text('Kirim',
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.black, fontSize: 18.0, )),
            ),
            accountEmail: new Text(""),
            currentAccountPicture: new CircleAvatar(
                backgroundImage: NetworkImage(
                    'https://s3.amazonaws.com/ionic-io-static/KMFRjXpQYyZ7DiB8IwJX_udin.jpeg')),
          ),
        )
      ],
    ),
  );
}
