import 'package:flutter/material.dart';

class KenRegister extends StatefulWidget {
  KenRegister({Key key, this.title}) : super(key: key);
  final String title;
  @override
  _KenRegisterPageState createState() => _KenRegisterPageState();
}

class _KenRegisterPageState extends State<KenRegister> {
  @override
  Widget build(BuildContext context) {
    TextStyle style = TextStyle(fontFamily: 'Montserrat', fontSize: 20.0);
    final tulisan = Padding(
      padding: EdgeInsets.all(8.0),
      child: Column(
        children: <Widget>[
          Text(
            'Already have account ?',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          InkWell(
            child: Text(
              'Sign In',
              style: TextStyle(
                  color: Colors.orange,
                  fontFamily: 'Montserrat',
                  fontWeight: FontWeight.bold,
                  decoration: TextDecoration.underline),
            ),
            onTap: () {
              Navigator.push(
                context, new
                MaterialPageRoute(
                  builder: (context) => KenRegister(title: 'Register'),
                ), //MaterialPageRoute
              );
            },
          )
        ],
      ),
    );
    final regisButon = new Material(
      elevation: 5.0,
      borderRadius: BorderRadius.circular(30.0),
      color: Color(0xFF9C0A00),
      child: MaterialButton(
        minWidth: MediaQuery.of(context).size.width,
        padding: EdgeInsets.fromLTRB(20.0, 15.0, 20.0, 15.0),
        onPressed: () {},
        child: Text("Register",
            textAlign: TextAlign.center,
            style: style.copyWith(
                color: Colors.white, fontWeight: FontWeight.bold)),
      ),
    );

    return new Scaffold(
      appBar: new AppBar(
        title: new Text(widget.title),
        actions: <Widget>[
          new IconButton(icon: const Icon(Icons.save), onPressed: () {})
        ],
      ),
      body: new Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 155.0,
            child: Image.asset(
              "assets/log.png",
              fit: BoxFit.contain,
            ),
          ),
          SizedBox(height: 15.0),
          new ListTile(
            leading: const Icon(Icons.person),
            title: new TextField(
              decoration: new InputDecoration(
                hintText: "User Name",
              ),
            ),
          ),
          new ListTile(
            leading: const Icon(Icons.phone),
            title: new TextField(
              decoration: new InputDecoration(
                hintText: "Phone",
              ),
            ),
          ),
          new ListTile(
            leading: const Icon(Icons.email),
            title: new TextField(
              decoration: new InputDecoration(
                hintText: "Email",
              ),
            ),
          ),
          const Divider(
            height: 1.0,
          ),
          new ListTile(
            leading: const Icon(Icons.lock),
            title: new TextField(
              decoration: new InputDecoration(
                hintText: "Password",
              ),
            ),
          ),
          new ListTile(
            leading: const Icon(Icons.lock),
            title: new TextField(
              decoration: new InputDecoration(
                hintText: "Confirm Password",
              ),
            ),
          ),
          SizedBox(height: 25.0),
          regisButon,
          SizedBox(
            height: 15.0,
          ),
          tulisan,
        ],
      ),
    );
  }
}
